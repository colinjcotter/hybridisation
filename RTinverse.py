"""
Code for building a DG RT inverse mass matrix.
"""

from firedrake import *

mesh = UnitSquareMesh(50,50)
RT = FiniteElement("RT",triangle,1)
bRT = BrokenElement(RT)
V = FunctionSpace(mesh,bRT)

u = TrialFunction(V)
v = TestFunction(V)

A = assemble(inner(u,v)*dx)

M = A.M.handle
I = M.duplicate()
I.zeroEntries()
D = M.duplicate()
D.zeroEntries()

D.setDiagonal(M.getDiagonal())
Dinv = D.duplicate()
v = M.getDiagonal()
v.reciprocal()
Dinv.setDiagonal(v)

Minv = I.copy()

#Iteratively solve
# D*v^{k+1} = r + (D-M)*v^k
# Write v^k = Minv^k r, with Minv^0 = I

# Then we have
# D*Minv^{k+1}*r = (I + (D-M)*Minv^k)*r
# So we get Minv^{k+1} by iterating
# D*Minv^{k+1} = I + (D-M)*Minv^k.
#i.e.,
# Minv^{k+1} = (D)^{-1} + (D)^{-1}*(D-M)*Minv^k.

#construct B = (D-M)

B = D.copy()
B.axpy(-1.0,M)

#create a function to test with
u = Function(V)
from numpy import random
u.dat.data[:] = random.randn(u.dat.data[:].size)

v = TestFunction(V)
RHS = assemble(inner(v,u)*dx)

with RHS.dat.vec_ro as v:
    r = v.copy()

u1 = Function(V)
up = r.copy()

for i in range(100):
    A = B.matMult(Minv)
    A = Dinv.matMult(A)
    Minv.zeroEntries()
    Minv.axpy(1.0,Dinv)
    Minv.axpy(1.0,A)

Minv.mult(r,up)
    
with u1.dat.vec as v:
    v.array[:] = up.array[:]

from numpy import abs as nabs
print nabs(u1.dat.data[:]).max()
print nabs(u1.dat.data[:]-u.dat.data[:]).max()

