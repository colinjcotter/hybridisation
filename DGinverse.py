"""
Code for building a DG inverse mass matrix.
"""

from firedrake import *

mesh = UnitSquareMesh(10,10)
V = FunctionSpace(mesh,"DG",1)

u = TrialFunction(V)
v = TestFunction(V)

A = assemble(u*v*dx)

One = Function(V).assign(1.0)

M = A.M.handle
I = M.duplicate()
I.zeroEntries()
ML_inv = M.duplicate()
ML_inv.zeroEntries()
ML = M.duplicate()
ML.zeroEntries()

LumpedA = assemble(v*One*dx)

with LumpedA.dat.vec_ro as v:
    Ones = v.duplicate()
    Ones.set(1.0)
    I.setDiagonal(Ones)
    ML.setDiagonal(v)

with LumpedA.dat.vec as v:
    v.reciprocal()

with LumpedA.dat.vec_ro as v:
    ML_inv.setDiagonal(v)

Minv = I.copy()

#Iteratively solve
# M_L*v^{k+1} = r + (M_L-M)*v^k
# Write v^k = Minv^k r, with Minv^0 = I

# Then we have
# M_L*Minv^{k+1}*r = (I + (M_L-M)*Minv^k)*r
# So we get Minv^{k+1} by iterating
# M_L*Minv^{k+1} = I + (M_L-M)*Minv^k.
#i.e.,
# Minv^{k+1} = (M_L)^{-1} + (M_L)^{-1}*(M_L-M)*Minv^k.

#construct B = (M_L-M)

B = ML.copy()
B.axpy(-1.0,M)

#create a function to test with
u = Function(V)
from numpy import random
u.dat.data[:] = random.randn(u.dat.data[:].size)

v = TestFunction(V)
RHS = assemble(v*u*dx)

with RHS.dat.vec_ro as v:
    r = v.copy()

u1 = Function(V)
up = r.copy()

for i in range(100):
    A = B.matMult(Minv)
    A = ML_inv.matMult(A)
    Minv.zeroEntries()
    Minv.axpy(1.0,ML_inv)
    Minv.axpy(1.0,A)

Minv.mult(r,up)
    
with u1.dat.vec as v:
    v.array[:] = up.array[:]

from numpy import abs as nabs
print nabs(u1.dat.data[:]-u.dat.data[:]).max()

